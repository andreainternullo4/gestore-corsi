import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import '../node_modules/bootstrap/dist/js/bootstrap.bundle.min.js'
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import { RouterProvider, createBrowserRouter } from 'react-router-dom'
import RegisterForm from './Components/RegisterForm/RegisterForm.jsx'
import RegisterContextProvider from './Components/Contexts/RegisterContextProvider.jsx'
import FetchPostRegister from './Components/Fetch/FetchPostRegister.jsx'
import FetchPostLogin from './Components/Fetch/FetchPostLogin.jsx'
import Main from './Components/Main/Main.jsx'
import UserMain from './Components/UserMain/UserMain.jsx'
import AdminMain from './Components/AdminMain/AdminMain.jsx'
import VisualizzaCorsi from './Components/VisualizzaCorsi/VisualizzaCorsi.jsx'
import CorsiBackend from './Components/Corsi/CorsiBackend/CorsiBackend.jsx'
import CorsiFrontend from './Components/Corsi/CorsiFrontend/CorsiFrontend.jsx'
import VisualizzaUtenti from './Components/VisualizzaUtenti/VisualizzaUtenti.jsx'

const router = createBrowserRouter([
  {
    path: "/",
    element: <RegisterContextProvider><App></App></RegisterContextProvider>,
    children: [
      {
        path: "/post",
        element: <FetchPostLogin></FetchPostLogin>
      }
    ]
  },
  {
    path: "/reg",
    element: <RegisterContextProvider><RegisterForm/></RegisterContextProvider>,
    children: [
      {
        path: "/reg/post",
        element: <FetchPostRegister/>
      }
    ]
  },
  {
    path: "/home",
    element: <RegisterContextProvider><Main/></RegisterContextProvider>,
  },
  {
    path: "/visualizzaCorsi",
    element: <RegisterContextProvider><VisualizzaCorsi/></RegisterContextProvider>
  },
  {
    path: "/CorsiBackend",
    element: <CorsiBackend/>
  },
  {
    path: "/CorsiFrontend",
    element: <CorsiFrontend/>
  },
  {
    path: "/VisualizzaUtenti",
    element: <VisualizzaUtenti/>
  }
])

ReactDOM.createRoot(document.getElementById('root')).render(
  <RouterProvider router={router} />
)
