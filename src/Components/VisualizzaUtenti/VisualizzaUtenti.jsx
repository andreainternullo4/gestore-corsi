import { Link, useNavigate } from "react-router-dom";
import Footer from "../Footer/Footer";
import Navbar from "../Navbar/Navbar";
import { Component, useContext } from "react";
import { RegisterContext } from "../Contexts/RegisterContext";
import { useState } from "react";

class VisualizzaUtenti extends Component {

    constructor(props) {
        super(props)
        this.state = {
            data: [],
        }
    }

    

    componentDidMount() {

        const fetchDataGet = async () => {
            const response = await fetch('http://localhost:8080/api/utente/getUtenti', {
                method: "GET"
            });

            const data = await response.json();
            
            if(Array.isArray(data)) {
                console.log(data)
                this.setState({data: data})
            } else {
                throw new Error('I dati ricevuti non sono nel formato previsto');
            }
        }

        fetchDataGet()
    }

    handleClick(mail, index) {
        const fetchDataDelete = async () => {
            const response = await fetch('http://localhost:8080/api/utente/deleteUtente', {
                method: "DELETE",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                  },
                body: JSON.stringify({email: mail})
            });

            if(response.ok){
                alert("utente eliminato con successo")
                this.setState(prevState => ({
                    data: prevState.data.filter((_, i) => i !== index)
                }));
            }
        }

        fetchDataDelete()
    }

    render() {

        const { data } = this.state

        return (  
            <>
                <Navbar/>

                <div className="container-fluid" style={{backgroundColor: "hsl(0, 0%, 96%)"}}>
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nome</th>
                                <th scope="col">Cognome</th>
                                <th scope="col">E-mail</th>
                                <th scope="col">Azioni</th>
                            </tr>
                        </thead>
                        <tbody>
                            {data.map((item, index) => (
                                <tr key={index}>
                                    <th scope="row" >{index + 1}</th>
                                    <td>{item.nome}</td>
                                    <td>{item.cognome}</td>
                                    <td>{item.email}</td>
                                    <td>
                                        <button className="btn btn-danger" onClick={() => this.handleClick(item.email, index)}>Elimina</button>
                                        <button className="btn btn-primary" type="submit">Modifica</button>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
                <Footer/>
            </>
        );
    }
}

export default VisualizzaUtenti;