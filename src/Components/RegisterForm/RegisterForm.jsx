import { Link } from "react-router-dom";
import { useContext } from "react";
import { RegisterContext } from "../Contexts/RegisterContext";
import FetchPostRegister from "../Fetch/FetchPostRegister";
import logo from '../../assets/logo.png'
import Footer from "../Footer/Footer";

function RegisterForm() {

    const {values, setValues, setRole, role} = useContext(RegisterContext)

    const handleInputChange = (e) => {
        const{name, value} = e.target;
        setValues({...values, [name]: value})
    }

    const handleSubmit = (e) => {
        e.preventDefault(),
        console.log(values)
    }

    return (  
        <>
        <div className="px-4 py-5 px-md-5 text-center justify-content-center align-items-center text-lg-start d-flex" style={{backgroundColor: "hsl(0, 0%, 96%)", height: "100vh"}}>
            <div className="container">
                <div className="row gx-lg-5 align-items-center">
                    <div className="col-lg-6 mb-5 mb-lg-0">
                        <img src={logo} alt="Logo" className="rounded mx-auto d-block h-30"></img>
                    </div>
                    <div className="col-lg-6 mb-5 mb-lg-0">
                        <div className="card">
                            <div className="card-body py-5 px-md-5">
                                <form onSubmit={handleSubmit}>
                                    <div className="row">
                                        <div className="col-md-6 mb-4">
                                            <div className="form-outline">
                                                <label htmlFor="exampleInputFirstName1" className="form-label">Nome</label>
                                                <input 
                                                    type="text" 
                                                    name="nome" 
                                                    className="form-control" 
                                                    id="registerFirstName" 
                                                    aria-describedby="firstNameHelp" 
                                                    placeholder="nome"
                                                    value={values.nome}
                                                    onChange={handleInputChange}
                                                />
                                            </div>
                                        </div>
                                        <div className="col-md-6 mb-4">
                                            <div className="form-outline">
                                                <label htmlFor="exampleInputLastName1" className="form-label">Cognome</label>
                                                <input 
                                                    type="text" 
                                                    name="cognome" 
                                                    className="form-control" 
                                                    id="registerLastName" 
                                                    aria-describedby="LastNameHelp" 
                                                    placeholder="cognome" 
                                                    value={values.cognome}
                                                    onChange={handleInputChange}
                                                />
                                            </div>
                                        </div>
                                    </div>
                            
                                    <div className="form-outline mb-4">
                                        <label htmlFor="exampleInputEmail1" className="form-label">Email</label>
                                        <input 
                                            type="email" 
                                            name="email" 
                                            className="form-control" 
                                            id="registerEmail" 
                                            aria-describedby="emailHelp" 
                                            placeholder="email" 
                                            value={values.email}
                                            onChange={handleInputChange}
                                        />
                                        <div id="emailHelp" className="form-text">Non condivideremo la tua email</div>
                                    </div>
                                    <div className="form-outline mb-4">
                                        <label htmlFor="exampleInputPassword1" className="form-label">Password</label>
                                        <input 
                                            type="password" 
                                            name="password" 
                                            className="form-control" 
                                            id="registerPassword" 
                                            placeholder="password" 
                                            value={values.password}
                                            onChange={handleInputChange}
                                        />
                                    </div>
                                    <FetchPostRegister/>
                                    <p>Sei iscritto? <Link to="/">Accedi</Link></p>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <Footer></Footer>
        </>
    );
}

export default RegisterForm;