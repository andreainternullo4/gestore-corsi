import { useState } from "react";
import { RegisterContext } from "./RegisterContext";

function RegisterContextProvider({children}) {

    const[values, setValues] = useState({
        nome: "",
        cognome: "",
        email: "",
        password: ""
    })

    const[credentials, setCredentials] = useState({
        email: "",
        password: ""
    })

    const[ruoli, setRuoli] = useState('')

    const[cookie, setCookie] = useState('')

    const[corso, setCorso] = useState('')

    const[decoded, setDecoded] = useState('')

    return (  
        <>
        <RegisterContext.Provider value={{values, setValues, credentials, setCredentials, ruoli, setRuoli, cookie, setCookie, corso, setCorso, decoded, setDecoded}}>
            {children}
        </RegisterContext.Provider>
        </>
    );
}

export default RegisterContextProvider;