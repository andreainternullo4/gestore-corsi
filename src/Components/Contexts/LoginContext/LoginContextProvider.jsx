import { useState } from "react";
import { LoginContext } from "./LoginContext";

function LoginConetextProvider({children}) {

    const[values, setValues] = useState({
        email: "",
        password: ""
    })

    return (  
        <>
        <LoginContext.Provider value={{values, setValues}}>
            {children}
        </LoginContext.Provider>
        </>
    );
}

export default LoginConetextProvider;