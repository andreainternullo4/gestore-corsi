import { Link, useNavigate } from "react-router-dom";
import logo from "../../assets/logo.png"
import { useContext } from "react";
import { RegisterContext } from "../Contexts/RegisterContext";

function Navbar() {

    return (  
        <>
            <nav className="navbar navbar-expand-lg bg-body-tertiary">
                <div className="container-fluid">
                    <Link className="navbar-brand" to="/home">
                        <img src={logo} className="rounded mx-auto d-block h-10" style={{width: "150px"}}/>
                    </Link>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarNavDropdown">
                        <ul className="navbar-nav">
                            <li className="nav-item">
                                <Link className="nav-link" to="/VisualizzaUtenti">Visualizza Utenti</Link>
                            </li>
                            <li className="nav-item dropdown">
                            <a className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                Visualizza Corsi
                            </a>
                            <ul className="dropdown-menu">
                                <li><Link className="dropdown-item" to="/CorsiFrontend">Corsi Frontend</Link></li>
                                <li><Link className="dropdown-item" to="/CorsiBackend">Corsi Backend</Link></li>
                            </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </>
    );
}

export default Navbar;