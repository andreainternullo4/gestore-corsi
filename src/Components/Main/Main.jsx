import Navbar from "../Navbar/Navbar";
import { useContext } from "react";
import { RegisterContext } from "../Contexts/RegisterContext";
import AdminMain from "../AdminMain/AdminMain";
import UserMain from "../UserMain/UserMain";
import NotFound from "../NotFound/NotFound";



function Main() {

    const {cookie, ruoli} = useContext(RegisterContext)

    const handleClick = () => {
        console.log(cookie)
        console.log(ruoli)
    }

    return (  
        <>
            <Navbar/>
            {ruoli === 'Admin' ? <AdminMain/> : (ruoli === 'Utente' ? <UserMain/> : <NotFound/>)}
      
        </>
    );
}

export default Main;