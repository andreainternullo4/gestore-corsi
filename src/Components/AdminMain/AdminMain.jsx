import { useContext } from "react";
import { RegisterContext } from "../Contexts/RegisterContext";
import PersonalInformationcard from "../PersonalInformationCard/PersonalInformationCard";

function AdminMain() {

    const {cookies}=useContext(RegisterContext)

    return (  
        <>
            <PersonalInformationcard></PersonalInformationcard>
        </>
    );
}

export default AdminMain;