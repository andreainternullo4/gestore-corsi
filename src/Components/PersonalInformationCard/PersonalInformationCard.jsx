import { useContext } from "react";
import { RegisterContext } from "../Contexts/RegisterContext";
import AdminActionCard from "../AdminActionCard/AdminActionCard";
import Footer from "../Footer/Footer";

function PersonalInformationcard() {

    const {cookie, ruoli}=useContext(RegisterContext)

    return (  
        <>
            <section style={{backgroundColor: "#eee", height: "100vh"}}>
                <div className="container py-5">
                    <div className="row">
                    <div className="col-lg-4">
                        <div className="card mb-4">
                            <div className="card-body text-center">
                                <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-chat/ava3.webp" alt="avatar"
                                className="rounded-circle img-fluid" style={{width: "150px"}}/>
                                <h5 className="my-3">{cookie.nome + " " + cookie.cognome}</h5>
                                <p className="text-muted mb-1">{ruoli}</p>
                                <p className="text-muted mb-4">Bay Area, San Francisco, CA</p>
                                <div className="d-flex justify-content-center mb-2">
                                <button type="button" className="btn btn-primary">Follow</button>
                                <button type="button" className="btn btn-outline-primary ms-1">Message</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-8">
                        <div className="card mb-4">
                            <div className="card-body">
                                <div className="row">
                                <div className="col-sm-3">
                                    <p className="mb-0">Full Name</p>
                                </div>
                                <div className="col-sm-9">
                                    <p className="text-muted mb-0">{cookie.nome + " " + cookie.cognome}</p>
                                </div>
                                </div>
                                <hr/>
                                <div className="row">
                                <div className="col-sm-3">
                                    <p className="mb-0">Email</p>
                                </div>
                                <div className="col-sm-9">
                                    <p className="text-muted mb-0">{cookie.email}</p>
                                </div>
                                </div>
                                <hr/>
                                <div className="row">
                                <div className="col-sm-3">
                                    <p className="mb-0">Phone</p>
                                </div>
                                <div className="col-sm-9">
                                    <p className="text-muted mb-0">(097) 234-5678</p>
                                </div>
                                </div>
                                <hr/>
                                <div className="row">
                                <div className="col-sm-3">
                                    <p className="mb-0">Mobile</p>
                                </div>
                                <div className="col-sm-9">
                                    <p className="text-muted mb-0">(098) 765-4321</p>
                                </div>
                                </div>
                                <hr/>
                                <div className="row">
                                <div className="col-sm-3">
                                    <p className="mb-0">Address</p>
                                </div>
                                <div className="col-sm-9">
                                    <p className="text-muted mb-0">Bay Area, San Francisco, CA</p>
                                </div>
                                </div>
                            </div>
                        </div>
                        {ruoli === 'Admin' ? <AdminActionCard/> : null}
                    </div>
                </div>
            </div>            
        </section>
        <Footer></Footer>
        </>
    );
}

export default PersonalInformationcard;