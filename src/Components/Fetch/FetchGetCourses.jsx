import Cookies from "js-cookie";
import { useContext } from "react";
import { RegisterContext } from "../Contexts/RegisterContext";

function FetchGetCourses({num}) {

    const{setCorso}=useContext(RegisterContext)

    const fetchDataGet = async () => {
        const response = await fetch(`http://localhost:8080/api/corso/getcorsi-da?id_ca=${num}`, {
            method: "GET",
            headers: {
                "Authorization": "Bearer " + Cookies.get("token"),
            }
        });
        return response.json();
    }
    
    const get = async () => {
        const data = await fetchDataGet()
        setCorso(data)
        console.log(data)
    } 

    const getToken = () => {
        console.log(Cookies.get("token"))
    }

    return (  
        <>
        </>
    
    );
}

export default FetchGetCourses;