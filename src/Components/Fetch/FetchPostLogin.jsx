import { useContext } from "react"
import { RegisterContext } from "../Contexts/RegisterContext"
import { jwtDecode } from "jwt-decode"
import { useNavigate } from "react-router-dom"
import Cookies from "js-cookie"

function FetchPostLogin() {

    const{credentials, setRuoli, setCookie, setToken} = useContext(RegisterContext)

    const navigate = useNavigate()

    const fetchDataPost = async () => {
        const response = await fetch("http://localhost:8080/api/utente/login", {
            method: "POST",
            headers: {
                'content-type': 'application/json',
                "Access-Control-Allow-Origin" : "*", 
                "Access-Control-Allow-Credentials" : true 
            },
            body: JSON.stringify(credentials)
        })
        if(response.status !== 200) {
            alert("email o password errati")
        }
        return response.json()
    }

    const post = async () => {
        const data = await fetchDataPost()
        Cookies.set("token", data.token)
        const token = data.token

        let decoded = jwtDecode(token)        
        const ruolo = decoded.ruoli[0]
        setRuoli(ruolo)
        console.log(decoded.ruoli[0])

        const cookieValue = Cookies.get("token")
        decoded = jwtDecode(cookieValue)
        setCookie(decoded)
        navigate('/home') 

        
    }

    return (  
        <>
            <button className="btn btn-primary" onClick={() => {post()}}>invia</button>
        </>
    );
}

export default FetchPostLogin;