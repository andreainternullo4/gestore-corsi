import { useContext } from "react";
import { RegisterContext } from "../Contexts/RegisterContext";
import { Link } from "react-router-dom";


function FetchPostRegister() {

    const {values} = useContext(RegisterContext)

    const fetchDataPost = async () => {
        const response = await fetch("http://localhost:8080/api/utente/registrazione", {
            method: "POST",
            headers: {
                'content-type': 'application/json',
                "Access-Control-Allow-Origin" : "*", 
                "Access-Control-Allow-Credentials" : true 
            },
            body: JSON.stringify(values)
        })
        if(response.ok) {
            const modal = document.getElementById("modal")
            modal.style.display = "block"           
        }
        return await response.json()
    }

    const post = async () => {
        const data = await fetchDataPost()
        console.log(data)
    }

    return (
        <>
            <button className="btn btn-primary btn-block mb-4" onClick={() => {post()}}>invia</button>
            <div className="modal" id="modal" tabIndex="-1" style={{display: "none"}}>
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Registrazione avvenuta con successo</h5>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div className="modal-body">
                            <p>Clicca Login per accedere ai servizi</p>
                        </div>
                        <div className="modal-footer">
                        <p>Clicca qui per il <Link to="/">Login</Link></p>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
    
}

export default FetchPostRegister;