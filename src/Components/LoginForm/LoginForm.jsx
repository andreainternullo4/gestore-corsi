import { useContext } from "react";
import { Link } from "react-router-dom";
import FetchPostLogin from "../Fetch/FetchPostLogin";
import { RegisterContext } from "../Contexts/RegisterContext";
import logo from '../../assets/logo.png'
import Footer from "../Footer/Footer";



function LoginForm() {

    const{credentials, setCredentials} = useContext(RegisterContext)

    const handleInputChange = (e) => {
        const{name, value} = e.target;
        setCredentials({...credentials, [name]: value})
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        console.log(credentials)        
    }

    return (  
        <>
        <div className="container-fluid px-4 py-5 px-md-5 text-center text-lg-start d-flex align-items-center" style={{backgroundColor: "hsl(0, 0%, 96%)", height: "100vh"}}>
            <div className="container">
                <div className="row gx-lg-5 align-items-center">
                    <div className="col-lg-6 mb-5 mb-lg-0">
                        <img src={logo} alt="Logo" className="rounded mx-auto d-block h-30"></img>
                    </div>
                    <div className="col-lg-6 mb-5 mb-lg-0">
                        <div className="card">
                            <div className="card-body py-5 px-md-5">
                                <form onSubmit={handleSubmit}>
                                    <div className="form-outline mb-4">
                                        <label htmlFor="exampleInputEmail1" className="form-label">Email</label>
                                        <input 
                                            type="email" 
                                            name="email" 
                                            className="form-control" 
                                            id="exampleInputEmail1" 
                                            aria-describedby="emailHelp" 
                                            placeholder="email" 
                                            value={credentials.email}
                                            onChange={handleInputChange}
                                        />
                                    </div>
                                    <div className="form-outline mb-4">
                                        <label htmlFor="exampleInputPassword1" className="form-label">Password</label>
                                        <input 
                                            type="password" 
                                            name="password" 
                                            className="form-control" 
                                            id="exampleInputPassword1" 
                                            placeholder="password" 
                                            value={credentials.password}
                                            onChange={handleInputChange}
                                        />
                                    </div>
                                    <FetchPostLogin/>
                                    <p>Non sei iscritto? <Link to="/reg">Registrati</Link></p>
                                </form>                    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <Footer></Footer>
        </>
    );
}

export default LoginForm;