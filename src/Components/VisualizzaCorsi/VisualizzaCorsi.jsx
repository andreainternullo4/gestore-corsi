import FetchGetCourses from "../Fetch/FetchGetCourses";
import Footer from "../Footer/Footer";
import Navbar from "../Navbar/Navbar";

function VisualizzaCorsi() {
    return (  
        <>
            <Navbar/>
            <div className="container-fluid" style={{height: "100vh"}}>
                <FetchGetCourses/>
            </div>
            <Footer/>
        </>
    );
}

export default VisualizzaCorsi;