import { Component, useContext } from "react";
import Cookies from "js-cookie";
import { RegisterContext } from "../../Contexts/RegisterContext";
import Navbar from "../../Navbar/Navbar";
import Footer from "../../Footer/Footer";
import { useLocation } from "react-router-dom";

class CorsiFrontend extends Component {
    

    constructor(props) {
        super(props)
        this.state = {
            data: [],
        };
    }

    componentDidMount() { 

        const fetchDataGet = async () => {

            const response = await fetch('http://localhost:8080/api/corso/getcorsi-da?id_ca=1', {
                method: "GET",
                headers: {
                    "Authorization": "Bearer " + Cookies.get("token"),
                }
            });
            const data = await response.json();
            if(Array.isArray(data)) {
                this.setState({data: data})
            } else {
                throw new Error('I dati ricevuti non sono nel formato previsto');
            }
            
        }

        fetchDataGet()
    }

    render() {

        const { data } = this.state

        return(
            <>  
                <Navbar/>
                <div className="container-fluid" style={{backgroundColor: "hsl(0, 0%, 96%)", height: "100vh"}}>
                    {data.map(item => (
                        <div key={Math.random().toString(36).substring(2, 9)} class="card" style={{marginBottom: "10px"}}>
                            <div className="card-header">
                                {item.nome_corso}
                            </div>
                            <div className="card-body">
                                <blockquote className="blockquote mb-0">
                                <p>{item.descrizione_completa}</p>
                                </blockquote>
                            </div>
                        </div>
                    ))}
                </div>
                <Footer/>
            </>
        )
    }

    

}
export default CorsiFrontend