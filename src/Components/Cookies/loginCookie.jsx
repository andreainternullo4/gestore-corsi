import Cookies from "js-cookie";
import { jwtDecode } from "jwt-decode";

export function loginCookie(jwtToken) {

    const jwtString = JSON.stringify(jwtToken.token)
    Cookies.set(cookieType.jwt, jwtString, {expires: jwtExpiration.oneMonth})
    return jwtDecode(jwtString)
}