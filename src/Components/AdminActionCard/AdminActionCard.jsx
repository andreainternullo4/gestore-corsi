import { Link } from 'react-router-dom';

function AdminActionCard() {
    return (  
        <>
            <div className="row">
                <div className="col-md-12">
                    <div className="card mb-4 mb-md-0">
                        <div className="card-body">
                            <p className="mb-4"><span className="text-primary font-italic me-1">Azioni eseguibili</span></p>
                            <div className="btn-group">
                                <button type="button" className="btn btn-white dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                                    Visualizza Corsi
                                </button>
                                <ul className="dropdown-menu">
                                    <li><Link className="dropdown-item" to="/corsiBackend">Corsi Backend</Link></li>
                                    <li><Link className="dropdown-item" to="/corsiFrontend">Corsi Frontend</Link></li>
                                </ul>
                            </div>
                            <div className="progress rounded" style={{height: "3px"}}>
                            <br></br>
                            </div>
                            <Link to="/VisualizzaUtenti">
                                <button type="button btn-white" className="btn">
                                    Visualizza Utenti
                                </button>
                            </Link>
                            <div className="progress rounded" style={{height: "3px"}}>
                            <br></br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default AdminActionCard;